import Vue from 'vue';
import Router from 'vue-router';
import Task from "../components/Task";
import TaskCreate from "../components/TaskCreate";

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'task-list',
      component: Task
    },
    {
      path: '/create',
      name: 'task-create',
      component: TaskCreate
    },
  ]
});

export default router;
