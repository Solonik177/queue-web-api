import Vue from 'vue';
import Vuetify from 'vuetify';
import App from './App.vue';
import router from './router';
import 'vuetify/dist/vuetify.min.css';

require('./app.scss');

Vue.config.productionTip = false;

Vue.use(Vuetify);

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
