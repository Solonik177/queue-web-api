<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\VirtualProperty;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Queue.
 *
 * @ORM\Table(name="queue")
 * @ORM\Entity(repositoryClass="App\Repository\QueueRepository")
 */
class Queue
{
    const STATUS_IN_QUEUE = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_COMPLETED = 3;

    const STATUS_MATRIX = [
        self::STATUS_IN_QUEUE => 'in queue',
        self::STATUS_IN_PROGRESS => 'in progress',
        self::STATUS_COMPLETED => 'completed',
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"api"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank
     * @ORM\Column(name="command", type="string", length=255)
     * @Groups({"api"})
     */
    private $command;

    /**
     * @var int
     * @ORM\Column(name="priority", type="integer")
     * @Groups({"api"})
     */
    protected $priority;

    /**
     * @var int
     * @ORM\Column(name="status", type="integer")
     * @Groups({"api", "status"})
     */
    protected $status;

    /**
     * @var Processor
     * @ORM\ManyToOne(targetEntity="App\Entity\Processor")
     * @ORM\JoinColumn(name="processor_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $processor;

    /**
     * @var Submitter
     * @ORM\ManyToOne(targetEntity="App\Entity\Submitter")
     * @ORM\JoinColumn(name="submitter_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $submitter;

    /**
     * @var float
     * @ORM\Column(name="processing_time", type="float", nullable=true)
     * @Groups({"api"})
     */
    protected $processingTime;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_created", type="datetime")
     * @Groups({"api"})
     */
    private $dateCreated;

    public function __construct()
    {
        $this->status = self::STATUS_IN_QUEUE;
        $this->dateCreated = new \DateTime("now");
    }

    public function __toString()
    {
        return $this->getCommand();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @param string $command
     */
    public function setCommand(string $command)
    {
        $this->command = $command;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Processor
     */
    public function getProcessor()
    {
        return $this->processor;
    }

    /**
     * @param Processor $processor
     * @return $this
     */
    public function setProcessor(Processor $processor)
    {
        $this->processor = $processor;
        return $this;
    }

    /**
     * @return Submitter
     */
    public function getSubmitter()
    {
        return $this->submitter;
    }

    /**
     * @param Submitter $submitter
     * @return $this
     */
    public function setSubmitter(Submitter $submitter)
    {
        $this->submitter = $submitter;
        return $this;
    }

    /**
     * @return float
     */
    public function getProcessingTime()
    {
        return $this->processingTime;
    }

    /**
     * @param $processingTime
     * @return $this
     */
    public function setProcessingTime($processingTime)
    {
        $this->processingTime = $processingTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     * @return $this
     */
    public function setDateCreated(\DateTime $dateCreated)
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }

    /**
     * @VirtualProperty
     * @SerializedName("processor_id")
     * @Groups({"api"})
     */
    public function getProcessorId()
    {
        return ($this->processor instanceof Processor) ? $this->processor->getId() : null;
    }

    /**
     * @VirtualProperty
     * @SerializedName("submitter_id")
     * @Groups({"api"})
     */
    public function getSubmitterId()
    {
        return ($this->submitter instanceof Submitter) ? $this->submitter->getId() : null;
    }

    /**
     * @VirtualProperty
     * @SerializedName("status_name")
     * @Groups({"status"})
     */
    public function getStatusName()
    {
        return (key_exists($this->status, self::STATUS_MATRIX)) ? self::STATUS_MATRIX[$this->status] : null;
    }
}
