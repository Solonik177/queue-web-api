<?php

namespace App\Form;

use App\Entity\Processor;
use App\Entity\Queue;
use App\Entity\Submitter;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QueueForm.
 */
class QueueForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'command',
                TextType::class,
                [
                   'property_path' => "command"
                ]
            )
            ->add(
                'priority',
                IntegerType::class,
                [
                   'property_path' => "priority"
                ]
            )
            ->add(
                'processor_id',
                EntityType::class,
                [
                    'class' => Processor::class,
                    'property_path' => "processor"
                ]
            )
            ->add(
                'submitter_id',
                EntityType::class,
                [
                    'class' => Submitter::class,
                    'property_path' => "submitter"
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => Queue::class,
                'csrf_protection' => false,
            )
        );
    }
}
