<?php

namespace App\Controller\Api;

use App\Entity\Processor;
use App\Entity\Queue;
use App\Entity\Submitter;
use App\Form\QueueForm;
use App\Repository\ProcessorRepository;
use App\Repository\QueueRepository;
use App\Repository\SubmitterRepository;
use App\Util\FormUtil;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Proxies\__CG__\App\Entity\Publisher\Site;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaskController extends AbstractFOSRestController
{
    /**
     * Get the next available task with highest priority.
     *
     * @Rest\Get("/task")
     *
     * @throws \Exception
     * @return object
     */
    public function getNextTaskAction()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var QueueRepository $queueRepository */
        $queueRepository = $em->getRepository(Queue::class);

        /** @var Queue $task */
        $task = $queueRepository->getNextHighPriorityTask();

        if ($task instanceof Queue) {
            $task->setStatus(Queue::STATUS_IN_PROGRESS);
            $em->persist($task);
            $em->flush();
        }

        $view = $this->view($task);

        $view->getContext()->setGroups(['api']);

        return $this->handleView($view);
    }

    /**
     * Submit a new task to the queue.
     * @param Request $request
     *
     * @Rest\Post("/task")
     * @return object
     */
    public function submitTaskAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $task = new Queue();
        $form = $this->createForm(QueueForm::class, $task);

        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em->persist($task);
            $em->flush();

            $view = $this->view(
                [
                    'data' => $task,
                ],
                Response::HTTP_OK
            );
            $view->getContext()->setGroups(['api']);
        } else {
            $formUtil = new FormUtil();
            $view = $this->view([
                'errors' => $formUtil->getErrorsFromForm($form),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Get the status of the task with id = $id
     *
     * @param int $id
     *
     * @Rest\GET("/task/{id}")
     *
     * @return object
     * @throws NotFoundHttpException
     */
    public function getStatusAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $task = $em->getRepository(Queue::class)->find($id);

        if (!$task instanceof Queue) {
            throw new NotFoundHttpException();
        }

        $view = $this->view($task);

        $view->getContext()->setGroups(['status']);

        return $this->handleView($view);
    }

    /**
     * Delete task with id = $id
     *
     * @param int $id
     *
     * @Rest\Delete("/task/{id}")
     *
     * @return object
     * @throws NotFoundHttpException
     */
    public function deleteTaskAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $task = $em->getRepository(Queue::class)->find($id);

        if (!$task instanceof Queue) {
            throw new NotFoundHttpException();
        }

        $em->remove($task);
        $em->flush();

        $view = $this->view([
            'status' => Response::HTTP_OK
        ]);

        return $this->handleView($view);
    }

    /**
     * Change status of task to completed
     *
     * @param int $id
     *
     * @Rest\Put("/task/{id}")
     *
     * @return object
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function handleCompleteTaskAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $task = $em->getRepository(Queue::class)->find($id);

        if (!$task instanceof Queue) {
            throw new NotFoundHttpException();
        }

        $task->setStatus(Queue::STATUS_COMPLETED);
        $now = new \DateTime("now");
        $task->setProcessingTime($now->getTimestamp() - $task->getDateCreated()->getTimestamp());
        $em->persist($task);
        $em->flush();

        $view = $this->view(
            [
                'data' => $task,
            ],
            Response::HTTP_OK
        );

        return $this->handleView($view);
    }

    /**
     * Get List of Tasks
     *
     * @Rest\Get("/tasks")
     *
     * @throws \Exception
     * @return object
     */
    public function getListTasksAction()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var QueueRepository $queueRepository */
        $queueRepository = $em->getRepository(Queue::class);

        /** @var Queue $task */
        $tasks = $queueRepository->findAll();

        $view = $this->view($tasks);

        $view->getContext()->setGroups(['api', 'status']);

        return $this->handleView($view);
    }

    /**
     * Get List of Processors
     *
     * @Rest\Get("/processors")
     *
     * @throws \Exception
     * @return object
     */
    public function getListProcessorsAction()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var ProcessorRepository $processorRepository */
        $processorRepository = $em->getRepository(Processor::class);

        /** @var Processor $processors */
        $processors = $processorRepository->findAll();

        $view = $this->view($processors);

        $view->getContext()->setGroups(['api']);

        return $this->handleView($view);
    }

    /**
     * Get List of Submitters
     *
     * @Rest\Get("/submitters")
     *
     * @throws \Exception
     * @return object
     */
    public function getListSubmittersAction()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var SubmitterRepository $submitterRepository */
        $submitterRepository = $em->getRepository(Submitter::class);

        /** @var Processor $submitters */
        $submitters = $submitterRepository->findAll();

        $view = $this->view($submitters);

        $view->getContext()->setGroups(['api']);

        return $this->handleView($view);
    }
}
