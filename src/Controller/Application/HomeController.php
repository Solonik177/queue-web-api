<?php

namespace App\Controller\Application;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route("/", name="app_index")
     * @Route("/create", name="app_vue", requirements={"route" = "^(?!.*_wdt|_profiler).+"})
     */
    public function index()
    {
        return $this->render('app.html.twig');
    }
}
