<?php

namespace App\DataFixtures;

use App\Entity\Processor;
use App\Entity\Queue;
use App\Entity\Submitter;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Create a test processor
        $processor = new Processor();
        $processor->setName("mail_processor");
        $manager->persist($processor);

        // Create a test submitter
        $submitter = new Submitter();
        $submitter->setName("test");
        $manager->persist($submitter);

        // Create a test queue
        $queue = new Queue();
        $queue->setSubmitter($submitter);
        $queue->setProcessor($processor);
        $queue->setCommand("echo 123");
        $queue->setPriority(555);
        $manager->persist($queue);

        // Create a test queue
        $queue = new Queue();
        $queue->setSubmitter($submitter);
        $queue->setProcessor($processor);
        $queue->setCommand("echo 321");
        $queue->setPriority(1);
        $manager->persist($queue);

        $manager->flush();
    }
}
