let Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addEntry('app', './assets/js/app.js')
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader(function (sassOptions) {
    }, {
        resolveUrlLoader: false,
    })
    .enableVueLoader();

const config = Encore.getWebpackConfig();

if(Encore.isProduction()) {
    config.resolve.alias = {
        vue: 'vue/dist/vue.min.js',
    };
}

module.exports = config;
